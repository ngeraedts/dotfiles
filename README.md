# Dotfiles

A collection of dotfiles to be used with [GNU Stow](https://www.gnu.org/software/stow/).

```
  _______________________
 / Moo. Dotfiles.        \
 \_______________________/
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||```