#!/bin/zsh

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

zstyle ':omz:update' mode reminder # just remind me to update when it's time

ZSH_THEME="pi"
ENABLE_CORRECTION="true"
HYPHEN_INSENSITIVE="true"
DISABLE_UNTRACKED_FILES_DIRTY="true"
HIST_STAMPS="yyyy-mm-dd"
ZSH_CUSTOM="${HOME}/dotfiles/zsh/custom"

plugins=(
  git
  zsh-autosuggestions
  fzf-zsh-plugin
)

source $ZSH/oh-my-zsh.sh

# Homebrew
eval "$(/opt/homebrew/bin/brew shellenv)"

# Prevent Homebrew from auto-updating at inopportune times
export HOMEBREW_NO_AUTO_UPDATE=1

# Environment Customizations
export PATH=$HOME/.local/bin:$PATH

# Ensure cargo is available (rust toolchain)
export PATH=$HOME/.cargo/bin:$PATH

# Only autocorrect commands, not arguments
unsetopt correct_all
setopt correct

# Aliases
alias ggs-qa="echo export AWS_PROFILE=ggs-qa && export AWS_PROFILE=ggs-qa"
alias ggs-apricity="echo setting AWS_PROFILE=ggs-apricity && export AWS_PROFILE=ggs-apricity"

alias treeall="tree --dirsfirst -C -I \".git\""
alias tree="tree --dirsfirst -C -I \".git|*.pyc|*.egg-info|build|__pycache__|venv*|deps|_build|node_modules|elm-stuff|vendor\""
alias ls="gls --group-directories-first --color=auto"
alias gs="git status"

# Project aliases
alias greensync="cd ~/code/enbala/greensync-adapter"
alias switchdin="cd ~/code/enbala/switchdin-adapter"
alias vpp="cd ~/code/enbala/vpp"

eval "$(direnv hook zsh)"
eval "$(zoxide init --cmd=cd zsh)"

# mise veresion manager
eval "$(mise activate zsh)"
export PATH="$HOME/.local/share/mise/shims:$PATH"

# Temp Alias while ElixirLS adds support for mise
alias rtx=mise

# Add to shell profile
export ERL_AFLAGS="-kernel shell_history enabled"
