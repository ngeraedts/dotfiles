" ==========================================================
set nocompatible

" ==========================================================
" Plugins
" ==========================================================

" Auto-install vim-plug
if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
		\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
Plug 'elixir-editors/vim-elixir'     " Elixir
Plug 'godlygeek/tabular'             " Character alignment helper
Plug 'lepture/vim-jinja'
Plug 'martinda/Jenkinsfile-vim-syntax'
Plug 'pbrisbin/vim-cram'
Plug 'scrooloose/nerdcommenter'      " Easy commenting
Plug 'scrooloose/syntastic'          " Syntastic
Plug 'sheerun/vim-polyglot'          " Languages
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-vinegar'             " Enhanced file navigation
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-scripts/groovyindent-unix' " Groovy
Plug 'zanloy/vim-colors-sunburst'    " Colorscheme
Plug 'ajh17/VimCompletesMe'          " Tab Completion
call plug#end()

set modeline
set modelines=5

set autoread                         " auto reload a file when edited externally
set backspace=indent,eol,start       " make backspace work as expected
set guitablabel=(%N%M)\ %f           " tab label format
set history=50                       " command history
set hlsearch                         " highlight search matches
set incsearch                        " jump to search match while typing
set nobackup                         " disable ~backup files
set wildmode=longest,list            " Use Bash-style tab completion
set path+=**
set wildmenu
set showmatch
set number
set scrolloff=10
set directory=$HOME/.vim/swapfiles// " write swapfiles outside of working directory
set backupdir=$HOME/.vim/backups//   " write backup files outside of working directory
set foldmethod=syntax
set foldnestmax=10
set nofoldenable
set foldlevel=2

" Splits below and to the right
set splitright
set splitbelow

set smarttab
set ignorecase
set smartcase
set mouse=a

" Visual settings
set background=dark

" Global format options
set textwidth=79
set formatoptions-=t
set formatoptions+=c1j

silent! colorscheme slate
silent! colorscheme sunburst

" Vinegar configuration
let &wildignore = '*.swo,*.swp,*.pyc,*.o'

" Default whitespace settings (additional settings in ~/.vim/ftplugin)
set ts=4 sw=4 sts=4 expandtab

" Simplier split navigation
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

"autoindent line
nnoremap <leader>= ==

"syntax highlighting
syntax on

" Functions
fun! StripTrailingWhitespace()
	" Only strip if the b:noStripeWhitespace variable isn't set
	if exists('b:noStripWhitespace')
		return
	endif
	%s/\s\+$//e
endfunction

" Show all tabstops explicitly
noremap <F5> :set list!<CR>
inoremap <F5> <C-o>:set list!<CR>
cnoremap <F5> <C-c>:set list!<CR>
set listchars=tab:>-


" Automatically clear trailing whitespace (except specific languages)
au FileType markdown let b:noStripWhitespace=1
au BufWritePre * call StripTrailingWhitespace()

" Automatically resize splits when window resized
au VimResized * wincmd =

" Highlight unwanted whitespace (group defined in ftplugin files)
:hi ExtraWhitespace ctermbg=darkgreen guibg=darkgreen

let g:go_version_warning = 0

" Syntastic configuration
let g:syntastic_always_populate_loc_list = 0
let g:syntastic_auto_loc_list = 1
let g:syntastic_loc_list_height = 4
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_aggregate_errors = 1
let g:syntastic_python_checkers = ['flake8']
let g:syntastic_quiet_messages = {"regex": 'E303'}
let g:syntastic_rst_checkers=['sphinx']

" nerdcommenter configuration
let g:NERDSpaceDelims = 1
let g:NERDDefaultAlign = 'left'

" Airline configuration
let g:airline_theme = "minimalist"

" vim-go
let g:go_fold_enable = []

" Filetype settings
filetype plugin on
